C  Copyright (C) 2018  Riccardo Riolo
C
C  This program is free software: you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation, either version 1, or (at your option)
C  any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program. If not, see <https://www.gnu.org/licenses/>.
C
      SUBROUTINE DCRCFT(N, X, Y, R, S, IERR)
C  Calculate the radius of the circle that best fit the points
C  minimizing the algebraic distance.
C
C  Parameters:
C     N      (input) number of data points.
C     X      (input) abscissae of each point.
C     Y      (input) ordinates of each point.
C     R      (output) radius of the circle.
C     S      (output) standard deviation of R.
C     IERR   (output) error flag.
C            IERR = 0 (no errors)
C            IERR = -1 (if N.LE.2)
C
C  DECLARE ARGUMENTS.
      INTEGER N, IERR
      DOUBLE PRECISION X(*), Y(*), R, S
C  DECLARE LOCAL VARIABLES.
      INTEGER I
      DOUBLE PRECISION XM, YM, U, V, D,
     *    AU, AUV, AV, BU, BV
C  VALIDITY-CHECK ARGUMENTS.
      IF ( N.LE.2 ) GO TO 101
C  OK, GO ON.
      IERR = 0
C  MEAN.
      XM = 0
      YM = 0
      DO 10 I = 1, N
          XM = XM + (X(I) - XM) / I
          YM = YM + (Y(I) - YM) / I
   10 CONTINUE
C  COEEFICIENTS.
      AU = 0
      AUV = 0
      AV = 0
      BU = 0
      BV = 0
      DO 20 I = 1, N
          U = X(I) - XM
          V = Y(I) - YM
          AU = AU + U*U
          AUV = AUV + U*V
          AV = AV + V*V
          BU = BU + (U**3 + U*V*V)/2.0
          BV = BV + (V**3 + U*U*V)/2.0
   20 CONTINUE
C  FIND CENTER.
      D = AU*AV - AUV**2
      U = (BU*AV - BV*AUV) / D
      V = (BV*AU - BU*AUV) / D
C  FIND RADIUS, CALCULATE MEAN AND STD DEV.
      R = 0
      S = 0
      DO 30 I = 1, N
          D = SQRT((U+XM-X(I))**2 + (V+YM-Y(I))**2)
          AU = D - R
          R = R + AU / I
          S = S + AU * (D - R)
   30 CONTINUE
      S = SQRT(S/(N-1))
C  NORMAL RETURN.
  100 CONTINUE
      RETURN
C  N.LE.2.
  101 CONTINUE
      IERR = -1
      RETURN
C  END SUBROUTINE DCRCFT.
      END
