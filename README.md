Circular fit
---
`FORTRAN 77` subroutine to fit circular data as described in
[link](https://dtcenter.org/met/users/docs/write_ups/circle_fit.pdf).

